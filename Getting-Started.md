# Getting started with custom kubernetes deployments from windows

## Pre-requisites

* Docker for windows (https://store.docker.com/editions/community/docker-ce-desktop-windows)
* Kubectl (https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-with-powershell-from-psgallery)
* Git (https://git-scm.com/downloads)
* Acs-engine source code (https://bitbucket.org/afawkes/acs-engine/src/master/)
  * You should create a fork of this repository for yourself as the easiest way to extract the files from the docker container is to commit them to the repo and push it back into bitbucket.

## First Steps

* Install Docker
  * Use linux containers as that is what we will compile acs-engine into
  * Open Docker for Windows, it will sit in your system tray where you can access the settings
  * You’ll need to get a docker cloud account (https://cloud.docker.com) which will let you log into docker on your local machine.
* Compile the acs-engine into a running docker container
  * In a powershell window navigate to the root of the acs-engine folder
  * From here run “.\scripts\devenv.ps1”
    * You must be in the root to run this command as it uses the dockerfile in this directory, if you navigate to the scripts folder and just run .\devenv.ps1 then it will fail as there is no dockerfile.
    * This will take a little while to run as it has to download and unpack the daemons, it then runs through the dockerfile and installs all the dependencies into the container
    * Towards the end you’ll get a request from docker to share drive, accept this
* When the installation completes you’ll be left in the shell of the container where we can move on to using the engine to create arm templates.
  * Run the command “make build”
    * This will build the acs-engine tool
      * When this command completes, you’ll have a bin folder containing th e acs-engine tool
      * There is also a folder called “examples” which contains api models used for the generation of arm templates for the various orchestration tools, we only care about kubernetes for the purposes of this setup and the kubernetes.json file is the one we will be working with.
* Firstly we will need a service principle and an ssh key to provide the api model.
  * Go and create a service principle in azure and assign it contributer rights to the subscription.  Generate a key and note down the client id and the key.
* We will also need to provide an RSA public key which can be generated using putty-gen
  * Putty gen produces the public key in the wrong format i.e. 

```txt
    ---- BEGIN SSH2 PUBLIC KEY ----
    Comment: "rsa-key-20180529"
    AAAAB3NzaC1yc2EAAAABJQAAAQEA96r4IgnanMyXctHpnxwFovD5D1WO314dDZCU
    TaiPLgrfRqU/eQW3T3tWB+ceitOOm3gxkjz3FAm9RQlgvNkFoFver7me0QqGAxph
    oy8wCzirBLxFRsH7R7/xTCXyjcLOTvJe33We1njTAlre0pljsWNXabnU7j1YMRKt
    Sktc/K/gfkuK7kb48dCPki/ZZkU0I6ATz180U1yxzDc68A4Ym0Oby90IElKoj0to
    VpR3h6R2hriL/N90wvKnCzc4F9b5hOVHn7Ex+yQJcKtGxpqUheesRtFhCKYcQ4VP
    ShXIbR9W4SJ/m059jdrC/M/A0PqoWUBw/V3U5muaOD91u4XNIw==
    ---- END SSH2 PUBLIC KEY ----
```

* We want to take the key part of this and put it into the format (no new line breaks and just a single space between ssh-rsa and the key)

```txt
Ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA96r4IgnanMyXctHpnxwFovD5D1WO314dDZCUTaiPLgrfRqU/eQW3T3tWB+ceitOOm3gxkjz3FAm9RQlgvNkFoFver7me0QqGAxphoy8wCzirBLxFRsH7R7/xTCXyjcLOTvJe33We1njTAlre0pljsWNXabnU7j1YMRKtSktc/K/gfkuK7kb48dCPki/ZZkU0I6ATz180U1yxzDc68A4Ym0Oby90IElKoj0toVpR3h6R2hriL/N90wvKnCzc4F9b5hOVHn7Ex+yQJcKtGxpqUheesRtFhCKYcQ4VPShXIbR9W4SJ/m059jdrC/M/A0PqoWUBw/V3U5muaOD91u4XNIw==
```

* With these we will be able to create a usable model from which to generate our arm templates which will deploy the cluster, these steps are important and the reason we are using the engine in this way, the arm templates for clusters are very complicated and having it handle all of the service principle and ssh interactions for us are one of the valuable parts.
* The easiest way to create this model is to create a json file I.e kube.json

```json
{
  "apiVersion": "vlabs",
  "properties": {
    "orchestratorProfile": {
      "orchestratorType": "Kubernetes"
    },
    "masterProfile": {
      "count": 1,
      "dnsPrefix": "helloworld",
      "vmSize": "Standard_D2_v2"
    },
    "agentPoolProfiles": [
      {
        "name": "agentpool1",
        "count": 3,
        "vmSize": "Standard_D2_v2",
        "availabilityProfile": "AvailabilitySet"
      }
    ],
    "linuxProfile": {
      "adminUsername": "azureuser",
      "ssh": {
        "publicKeys": [
          {
            "keyData": " Ssh-rsa AAAB3NzaC1yc2EAAAABJQAAAQEA96r4IgnanMyXctHpnxwFovD5D1WO314dDZCUTaiPLgrfRqU/eQW3T3tWB+ceitOOm3gxkjz3FAm9RQlgvNkFoFver7me0QqGAxphoy8wCzirBLxFRsH7R7/xTCXyjcLOTvJe33We1njTAlre0pljsWNXabnU7j1YMRKtSktc/K/gfkuK7kb48dCPki/ZZkU0I6ATz180U1yxzDc68A4Ym0Oby90IElKoj0toVpR3h6R2hriL/N90wvKnCzc4F9b5hOVHn7Ex+yQJcKtGxpqUheesRtFhCKYcQ4VPShXIbR9W4SJ/m059jdrC/M/A0PqoWUBw/V3U5muaOD91u4XNIw=="
          }
        ]
      }
    },
    "servicePrincipalProfile": {
      "clientId": "0DiEJ06HuCsu31y1z5LptmTG10zO5lifUNsybFNPrcc=",
      "secret": "qwefjkn23roin23roi23jf13oicn132iofm1p9omfi1n"
    }
  }
}
```

* You can save this adjacent to the acs-engine tool in the bin folder of the repository and then commit and push it up
* Then on the docker machine do a git pull to retrieve the file
* Now you can navigate to the bin folder and run the following command

```go
acs-engine generate kube.json
```

* These templates can now be used to deploy a kubernetes cluster to Azure, the parameters file can be amended to increase the number of agents in the pool, it can also be changed and redeployed and the new agents will be automatically picked up by the master server.
* Now in order to manage this cluster from your local machine, you will need to go and get the config file and store it locally for kubectl to use.  This will allow you to create a proxy to your local machine and access the kube GUI for the cluster which can provide easy to read information
  * Ensure you have the private key file (created earlier) in which to connect to the master server.
  * You can use WinSCP to retrieve the configuration file, connect to the public ip address of the machine via SSH and navigate to: "/home/*username*/.kube" and download "config".  This file contains all the connection configurations required by kubectl to remote to the master server, the contents of the file will look like the below:

```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: 
    *long key string*
    server: https://helloworld.uksouth.cloudapp.azure.com
  name: helloworld
contexts:
- context:
    cluster: helloworld
    user: helloworld-admin
  name: helloworld
- context:
    cluster: ""
    namespace: mystuff
    user: ""
  name: my-context
current-context: helloworld
kind: Config
preferences: {}
users:
- name: helloworld-admin
  user:
    client-certificate-data: 
    *long key string*
    client-key-data: 
    *long key string*
```

* On your local machine you can copy this config file to "%USERPROFILE%/.kube" and now when you run kubectl it will automatically default to the cluster you have just created
* Now run :

```bash
kubectl proxy
```

* Now if you connect to http://localhost:8001/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy on your local machine you will be able to navigate around the kubernetes UI.